# Cardiac Epione - Wellcoming Package

A collection of short guides to get you through your first few hours on the calculation/linux servers, mainly on NEF and IHUSUX.

|:exclamation: The guide is written from personal experiences and trail and errors.|
|---|


**<ins>Content:</ins>**
1. [Quick Start: Calculation Server 101](quick_start_calculation_server_101/)
	1. Get Access to Server
	2. Install `conda` and Python Packages
	3. Transferring File with `rsync`
	4. Running job with `oarsub`
	5. Running job on IHUSUX
	6. Moving file between servers with rsync

2. [Miscellaneous](misc/)
	1. Run Jupyter Notebook from server
	2. Ranger: Vim-key binding terminal File Browser
